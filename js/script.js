$(document).ready(() => {

    $('.navigation').hover(function () {
        this.classList.toggle('navigation__button_opened');
    });

    $('.navigation').hover(function () {
        $('.dropdown-menu').stop().slideToggle();
    });

});
